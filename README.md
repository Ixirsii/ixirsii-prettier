# @Ixirsii/Prettier Config

## About

Ixirsii's prettier config

## Usage

Add a dev dependency on the package
```
npm i -D @ixirsii/prettier-config
```

In your package.json add

```JSON
{
  "prettier": "@ixirsii/prettier-config",
}
```